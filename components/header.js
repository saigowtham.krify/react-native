import React, {useState,useEffect}from "react";
import Dialog, { DialogContent,DialogTitle,DialogFooter, DialogButton } from 'react-native-popup-dialog';
import BackgroundFetch from "react-native-background-fetch";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Button,
    TextInput
    
  } from 'react-native';
const Header=({ navigation })=>{
    const [popupState,setPopupState]=useState(false);
    const [popup1State,setPopup1State]=useState([]);
    const [count,setCount]=useState(0);
  const [message,setMessage]=useState('')
  useEffect(()=>{
    // setPopupState(true)
    initBackgroundFetch()
  },[])
   const  initBackgroundFetch =async()=> {
    // BackgroundFetch event handler.
    const onEvent = async (taskId) => {
      console.log('[BackgroundFetch] task: ', taskId);
      setCount(count+1);
      console.log(count,"Count variable")
      // Do your background work...
      await addEvent(taskId);
      // IMPORTANT:  You must signal to the OS that your task is complete.
      // BackgroundFetch.finish(taskId);
    }

    // Timeout callback is executed when your Task has exceeded its allowed running-time.
    // You must stop what you're doing immediately BackgroundFetch.finish(taskId)
    const onTimeout = async (taskId) => {
      console.warn('[BackgroundFetch] TIMEOUT task: ', taskId);
      BackgroundFetch.finish(taskId);
    }

    // Initialize BackgroundFetch only once when component mounts.
    let status = await BackgroundFetch.configure({minimumFetchInterval: 15}, onEvent, onTimeout);

    console.log('[BackgroundFetch] configure status: ', status);
  }
  const addEvent=(taskId)=> {

    // Simulate a possibly long-running asynchronous task with a Promise.
    return new Promise((resolve, reject) => {
      setPopup1State([...popup1State,{
        taskId:taskId+count,
        count:count
      }])
    
      // this.setState(state => ({
      //   events: [...state.events, {
      //     taskId: taskId,
      //     timestamp: (new Date()).toString()
      //   }]
      // }));
      resolve();
    });
  }
    return(
        <SafeAreaView>
      <ScrollView>
        <View
            style={{
            flexDirection: "row",
            height: 100,
            padding: 20
          }}
        >
     <View style={{ backgroundColor: "blue", width:110,marginRight:20 }} />
     <View style={{ backgroundColor: "red", width:110  }} /> 
     </View>
      <View style={{
          padding: 20}}>
        <Button title='VIEW NOW'/>
        <View><Text>message:{message}</Text></View>
        <Image source={{uri:"https://reactjs.org/logo-og.png"}}
          style={{ height: 100,marginTop:30 }}/>
      </View>
      <View style={{flexDirection:"row",padding: 20}}>
        <View style={{width:115}}>
        <Button title='Open Modal' onPress={()=>setPopupState(true)}/>
        </View>
        <View style={{marginLeft:15,width:115}}>
        <Button title='Footer' onPress={() => navigation.navigate('Footer',{
            itemId: 479,
            name:'Footer',
            message: message,
          })}/>
        </View>
      </View>
      <View>
        <View>
          <TextInput 
          style={{height: 40,borderColor: '#06bcee', borderBottomWidth: 1}}
          placeholder="Type here!"
          autoFocus={true}
          onChangeText={text => setMessage(text)}/>
        </View>
      {/* <Dialog
      visible={popupState}
      dialogTitle={<DialogTitle title=" sample Dialog " />}
      width={0.9}
      footer={
        <DialogFooter>
          <DialogButton
            text="OK"
            onPress={() => setPopupState(false)}
          />
        </DialogFooter>
      }
     >
      <DialogContent>
        <Text>Hello  World!</Text>
      </DialogContent>
      </Dialog> */}
      </View>
     </ScrollView>
     </SafeAreaView>
    )
}
export default Header