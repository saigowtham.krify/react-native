import React from "react";
import { View,Text,Button } from "react-native";
const Footer=({ navigation,route })=>{
    // const { itemId, name,message } = route.params;
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          {/* <Text>{name}</Text>
          <Text>{itemId}</Text>
          <Text>{message}</Text> */}
          <View style={{flexDirection: "row",marginTop:10}}>
              <View style={{width:115}}><Button title='Home' onPress={() => navigation.navigate('home')}/></View>
              <View style={{marginLeft:15,width:115}}><Button title='Goback' onPress={() => navigation.goBack()}/></View>
          
          </View>
        </View>
        
      );
}
export default Footer