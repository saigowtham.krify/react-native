/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Header from './components/header';
import Footer from './components/footer';
import {View,Text} from 'react-native'
const Settings=()=>{
  return(
    <View><Text>hello</Text></View>
  )
}
const App = ()=>{
  const Stack = createNativeStackNavigator();
  const Tabs=createBottomTabNavigator()
  return(
    <NavigationContainer>
      <Tabs.Navigator>
        <Tabs.Screen name="home"
         component={Header}
        //  ScreenOptions={({route})=>({
        //    tabBarIcon:({focused,color,size})=>{

        //    }
        //  })}
          />
        <Tabs.Screen name="Settings" component={Settings} />
        <Tabs.Screen name="Footer" component={Footer} />
      </Tabs.Navigator>
      {/* <Stack.Navigator 
       screenOptions={{
         headerStyle:{
           backgroundColor:"tomato"
         }
       }}>
        <Stack.Screen name="Header" 
        component={Header}
         />
        <Stack.Screen name="Footer" 
         options={{
          headerBackTitle:"Back"
         }}
        component={Footer} />
      </Stack.Navigator> */}
    </NavigationContainer>
  )
}



export default App;
